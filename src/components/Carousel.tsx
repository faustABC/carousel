import {
  FC,
  TouchEvent,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import styles from './carousel.module.scss';

export const CarouselItem: FC = () => (
  <h1 className={styles.carouselItem}>asd</h1>
);

export const Carousel = () => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const [isAutoScroll, setAutoScroll] = useState(true);
  const [currentTouchX, setCurrentTouchX] = useState<number | null>(null);
  const [initialTouchX, setInitialTouchX] = useState<number | null>(null);
  const [newIndex, setNewIndex] = useState<number | null>(null);

  const toggleScroll = useCallback(
    () => setAutoScroll(isScroll => !isScroll),
    [],
  );

  const onTouchStart = (e: TouchEvent<HTMLElement>) => {
    setInitialTouchX(e.touches[0].clientX);
    setCurrentTouchX(e.touches[0].clientX);
  };

  const onTouchMove = (e: TouchEvent<HTMLElement>) => {
    setCurrentTouchX(e.touches[0].clientX);
    console.log(currentTouchX, initialTouchX);
  };

  const onTouchEnd = () => {
    setInitialTouchX(null);
    setCurrentTouchX(null);
    const computedIndex = currentIndex + (newIndex ?? 0);
    const boundedIndex =
      computedIndex < 0 ? 0 : computedIndex > 2 ? 2 : computedIndex;
    setCurrentIndex(boundedIndex);
  };

  useEffect(() => {
    if (isAutoScroll) {
      const interval = setInterval(
        () => setCurrentIndex(i => (i + 1) % 3),
        1000,
      );

      return () => clearInterval(interval);
    }
  }, [isAutoScroll]);

  const shift = useMemo(() => (currentTouchX ?? 0) - (initialTouchX ?? 0), [
    currentTouchX,
    initialTouchX,
  ]);

  useEffect(() => {
    const elementShift = Math.floor((shift + 150) / 300);
    setNewIndex(-elementShift);
  }, [shift]);

  return (
    <>
      <aside
        className={styles.carousel}
        onTouchStart={onTouchStart}
        onTouchMove={onTouchMove}
        onTouchEnd={onTouchEnd}
      >
        <div
          style={{ marginLeft: `${-currentIndex * 300 + shift}px` }}
          className={styles.carouselThumb}
        />
        {Array(3)
          .fill(null)
          .map(() => (
            <CarouselItem />
          ))}
      </aside>
      <button onClick={toggleScroll}>Toggle AutoScroll</button>
    </>
  );
};
